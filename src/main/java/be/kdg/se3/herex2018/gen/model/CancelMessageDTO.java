package be.kdg.se3.herex2018.gen.model;

/**
 * A DTO class containing an orderId of the order that needs to be canceled.
 */
public class CancelMessageDTO {
    private final int OrderId;

    public CancelMessageDTO(int orderId) {
        OrderId = orderId;
    }

    public int getOrderId() {
        return OrderId;
    }
}
