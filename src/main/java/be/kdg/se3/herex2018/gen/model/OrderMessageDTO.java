package be.kdg.se3.herex2018.gen.model;

import java.util.List;

/**
 * A DTO class containing an orderId and other parameters of the order that needs to be created.
 */
public class OrderMessageDTO {
    private final int orderId;
    private final int customerId;
    private final int price;
    private final List<Product> items;

    public OrderMessageDTO(int orderId, int customerId, int price, List<Product> items) {
        this.orderId = orderId;
        this.customerId = customerId;
        this.price = price;
        this.items = items;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public int getPrice() {
        return price;
    }

    public List<Product> getItems() {
        return items;
    }
}
