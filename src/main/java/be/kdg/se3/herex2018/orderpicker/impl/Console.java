package be.kdg.se3.herex2018.orderpicker.impl;

import be.kdg.se3.herex2018.orderpicker.api.Formatter;
import be.kdg.se3.herex2018.orderpicker.api.MessageService;
import be.kdg.se3.herex2018.orderpicker.exception.CommunicationException;
import be.kdg.se3.herex2018.orderpicker.listeners.CancelMessageListener;
import be.kdg.se3.herex2018.orderpicker.listeners.OrderMessageListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Console implements MessageService {
    private static final Logger logger = LoggerFactory.getLogger(Console.class);
    private final Formatter formatter;

    public Console(Formatter formatter) {
        this.formatter = formatter;
    }

    @Override
    public void init(OrderMessageListener listener) throws CommunicationException {
        throw new CommunicationException("this message service cannot receive any messages, it is unnecessary to initialize it with a listener");
    }

    @Override
    public void init(CancelMessageListener listener) throws CommunicationException {
        throw new CommunicationException("this message service cannot receive any messages, it is unnecessary to initialize it with a listener");
    }

    @Override
    public void init() throws CommunicationException {
        try {
            logger.info("Initializing the output queue");
            System.out.println("Starting th output queue...");
        } catch(Exception e) {
            throw new CommunicationException("Cannot print to System.out", e);
        }
    }

    @Override
    public void sendMesssage(Object o) throws CommunicationException {
        try {
            logger.info("Sending a message");
            String message = formatter.wrap(o);
            logger.debug(String.format("Message content: %s", message));
            System.out.println(message);
        } catch(Exception e) {
            throw new CommunicationException("Cannot print to System.out", e);
        }

    }

    @Override
    public void shutdown() throws CommunicationException {
        try {
            logger.info("Shutting down the output queue");
            System.out.println("Shutting down the output queue");
        } catch(Exception e) {
            throw new CommunicationException("Cannot shut down to System.out", e);
        }
    }
}
