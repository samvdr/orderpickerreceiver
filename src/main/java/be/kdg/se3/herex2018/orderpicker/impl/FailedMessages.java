package be.kdg.se3.herex2018.orderpicker.impl;

import be.kdg.se3.herex2018.gen.model.OrderMessageDTO;
import be.kdg.se3.herex2018.orderpicker.Enricher;
import be.kdg.se3.herex2018.orderpicker.api.FailHolder;
import be.kdg.se3.herex2018.orderpicker.exception.CommunicationException;
import be.kdg.se3.herex2018.orderpicker.exception.ErrorMessageException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FailedMessages implements FailHolder, Runnable {
    private Map<Integer, OrderMessageDTO> orders;
    private Enricher orderEnricher;
    private static FailHolder instance;

    private FailedMessages(Enricher orderEnricher) {
        this.orders = new HashMap<>();
        this.orderEnricher = orderEnricher;
    }

    public synchronized static FailHolder getInstance(Enricher orderEnricher) {
        if (instance == null) {
            return new FailedMessages(orderEnricher);
        }
        return instance;
    }

    @Override
    public void addMessage(OrderMessageDTO order) {
        orders.put(order.getOrderId(), order);
    }

    @Override
    public boolean isMessageFailed(int orderId) {
        return orders.containsKey(orderId);
    }

    @Override
    public void removeOrder(int orderId) {
        orders.remove(orderId);
    }

    @Override
    public void tryAddLocationToOrders() throws CommunicationException, ErrorMessageException {
        Map<Integer, OrderMessageDTO> iterationOrders = orders;
        Set<Integer> keys = iterationOrders.keySet();
        orders = new HashMap<>();

        for (Integer key : keys) {
            orderEnricher.addLocations(iterationOrders.get(key));
        }
    }

    @Override
    public void run() {
        try {
            tryAddLocationToOrders();
        } catch (CommunicationException | ErrorMessageException e) {
            // TODO: 10/08/2018 log error
        }
    }
}
