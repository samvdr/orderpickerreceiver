package be.kdg.se3.herex2018.orderpicker;

public enum OrganizingType {
    SINGLE,
    GROUP
}
