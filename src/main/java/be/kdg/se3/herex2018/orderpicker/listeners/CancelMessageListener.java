package be.kdg.se3.herex2018.orderpicker.listeners;

import be.kdg.se3.herex2018.gen.model.CancelMessageDTO;

public interface CancelMessageListener {
    void onReceive(CancelMessageDTO messageDTO);
}
