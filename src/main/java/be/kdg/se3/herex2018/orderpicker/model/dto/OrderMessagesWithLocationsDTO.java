package be.kdg.se3.herex2018.orderpicker.model.dto;

import java.util.List;

public class OrderMessagesWithLocationsDTO {
    private final List<OrderMessageWithLocationsDTO> orders;

    public OrderMessagesWithLocationsDTO(List<OrderMessageWithLocationsDTO> orders) {
        this.orders = orders;
    }

    public List<OrderMessageWithLocationsDTO> getOrders() {
        return orders;
    }
}
