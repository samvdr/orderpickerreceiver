package be.kdg.se3.herex2018.orderpicker;

import be.kdg.se3.herex2018.gen.model.CancelMessageDTO;
import be.kdg.se3.herex2018.gen.model.OrderMessageDTO;
import be.kdg.se3.herex2018.orderpicker.api.FailHolder;
import be.kdg.se3.herex2018.orderpicker.api.MessageService;
import be.kdg.se3.herex2018.orderpicker.api.Organizer;
import be.kdg.se3.herex2018.orderpicker.exception.CommunicationException;
import be.kdg.se3.herex2018.orderpicker.exception.ErrorMessageException;
import be.kdg.se3.herex2018.orderpicker.impl.FailedMessages;
import be.kdg.se3.herex2018.orderpicker.listeners.CancelMessageListener;
import be.kdg.se3.herex2018.orderpicker.listeners.OrderMessageListener;
import be.kdg.se3.herex2018.orderpicker.model.OrderMessageWithLocations;
import be.kdg.se3.herex2018.orderpicker.model.dto.OrderMessagesWithLocationsDTO;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class OrderPicker implements OrderMessageListener, CancelMessageListener {
    private final MessageService orderChanel;
    private final MessageService cancelChanel;
    private final MessageService outputChanel;
    private final Enricher orderEnricher;
    private Organizer singleOrganizer;
    private Organizer groupOrganizer;
    private OrganizingType organizingType;
    private LocationCache cache;
    private FailHolder failedOrders;
    private long timeForCacheToBeCleared;

    public OrderPicker(MessageService orderChanel,
                       MessageService cancelChanel,
                       MessageService outputChanel, Enricher orderEnricher) {
        this.orderChanel = orderChanel;
        this.cancelChanel = cancelChanel;
        this.outputChanel = outputChanel;
        this.orderEnricher = orderEnricher;
        this.cache = LocationCache.getInstance();
        this.failedOrders = FailedMessages.getInstance(this.orderEnricher);
    }

    public void setTimeForCacheToBeCleared(long timeForCacheToBeCleared) {
        this.timeForCacheToBeCleared = timeForCacheToBeCleared;
    }

    public void setSingleOrganizer(Organizer singleOrganizer) {
        this.singleOrganizer = singleOrganizer;
    }

    public void setGroupOrganizer(Organizer groupOrganizer) {
        this.groupOrganizer = groupOrganizer;
    }

    public void setOrganizingType(OrganizingType organizingType) {
        this.organizingType = organizingType;
    }

    public void start() {
        init();
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(
                cache, 0, timeForCacheToBeCleared, TimeUnit.SECONDS
        );
    }

    private void init() {
        try {
            orderChanel.init((OrderMessageListener) this);
            cancelChanel.init((CancelMessageListener) this);
            outputChanel.init();
        } catch (CommunicationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReceive(CancelMessageDTO messageDTO) {
        if (groupOrganizer.isOrderPresent(messageDTO.getOrderId())) {
            groupOrganizer.removeOrder(messageDTO.getOrderId());
        } else if (failedOrders.isMessageFailed(messageDTO.getOrderId())) {
            failedOrders.removeOrder(messageDTO.getOrderId());
        } else {
            // TODO: 10/08/2018 log annulatie
//            outputChanel.sendMesssage(String.format("Order %d wants to be canceled", messageDTO.getOrderId()));
        }
    }

    @Override
    public void onReceive(OrderMessageDTO messageDTO) {
        OrderMessageWithLocations orderMessageWithLocations = null;
        OrderMessagesWithLocationsDTO orderMessagesWithLocationsDTO = null;
        try {
            orderMessageWithLocations = orderEnricher.addLocations(messageDTO);
            if (orderMessageWithLocations != null) {
                switch (organizingType) {
                    case SINGLE:
                        orderMessagesWithLocationsDTO = singleOrganizer.organize(orderMessageWithLocations);
                        break;
                    case GROUP:
                        orderMessagesWithLocationsDTO = groupOrganizer.organize(orderMessageWithLocations);
                        break;
                    default:
                        orderMessagesWithLocationsDTO = singleOrganizer.organize(orderMessageWithLocations);
                        break;
                }
                outputChanel.sendMesssage(orderMessagesWithLocationsDTO);
            }
        } catch (CommunicationException | ErrorMessageException e) {
            System.out.println(e.getMessage());
        }
    }
}
