package be.kdg.se3.herex2018.orderpicker.model;

import be.kdg.se3.herex2018.orderpicker.model.ProductWithLocation;

import java.util.List;

public class OrderMessageWithLocations {
    private final int orderId;
    private List<ProductWithLocation> items;

    public OrderMessageWithLocations(int orderId, List<ProductWithLocation> items) {
        this.orderId = orderId;
        this.items = items;
    }

    public int getOrderId() {
        return orderId;
    }

    public List<ProductWithLocation> getItems() {
        return items;
    }

    public void setItems(List<ProductWithLocation> items) {
        this.items = items;
    }
}
