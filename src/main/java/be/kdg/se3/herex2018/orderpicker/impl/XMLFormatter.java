package be.kdg.se3.herex2018.orderpicker.impl;

import be.kdg.se3.herex2018.orderpicker.api.Formatter;
import be.kdg.se3.herex2018.orderpicker.exception.FormatException;
import com.thoughtworks.xstream.XStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringWriter;

public class XMLFormatter implements Formatter {
    private final Logger logger = LoggerFactory.getLogger(XMLFormatter.class);
    private final XStream xStream = new XStream();

    @Override
    public String wrap(Object o) throws FormatException {
        StringWriter writer = new StringWriter();
        String message = null;

        logger.info("object is being wrapped into an xml string");
        try {
            message = xStream.toXML(o);

            logger.debug(String.format("message content: %s", message));
        } catch (Exception e) {
            throw new FormatException("something went wrong while wrapping an object to an XML message", e);
        }
        return message;
    }

    @Override
    public Object unwrap(String message) throws FormatException {
        Object object = new Object();

        logger.debug(String.format("message content: %s", message));
        try {
            object = xStream.fromXML(message);

            logger.info("DetectionMessage recieved");
        } catch (Exception e) {
            throw new FormatException("something went wrong while unwrapping a message", e);
        }

        return object;
    }
}
