package be.kdg.se3.herex2018.orderpicker.api;

import be.kdg.se3.herex2018.orderpicker.exception.CommunicationException;
import be.kdg.se3.herex2018.orderpicker.listeners.CancelMessageListener;
import be.kdg.se3.herex2018.orderpicker.listeners.OrderMessageListener;

public interface MessageService {
    void init(OrderMessageListener listener) throws CommunicationException;

    void init(CancelMessageListener listener) throws CommunicationException;

    void init() throws CommunicationException;

    void sendMesssage(Object o) throws CommunicationException;

    void shutdown() throws CommunicationException;
}
