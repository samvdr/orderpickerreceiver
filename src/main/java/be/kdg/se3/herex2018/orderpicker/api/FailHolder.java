package be.kdg.se3.herex2018.orderpicker.api;

import be.kdg.se3.herex2018.gen.model.OrderMessageDTO;
import be.kdg.se3.herex2018.orderpicker.exception.CommunicationException;
import be.kdg.se3.herex2018.orderpicker.exception.ErrorMessageException;

public interface FailHolder {
    void addMessage(OrderMessageDTO order);
    boolean isMessageFailed(int orderId);
    void removeOrder(int orderId);
    void tryAddLocationToOrders() throws CommunicationException, ErrorMessageException;
}
