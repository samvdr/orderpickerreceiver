package be.kdg.se3.herex2018.orderpicker.exception;

public class ErrorMessageException extends Throwable {
    public ErrorMessageException(String message, Throwable cause) {
        super(message, cause);
    }
}
