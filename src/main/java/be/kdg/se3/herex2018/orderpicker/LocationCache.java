package be.kdg.se3.herex2018.orderpicker;

import be.kdg.se3.herex2018.orderpicker.model.ProductWithLocation;
import be.kdg.se3.herex2018.orderpicker.model.dto.LocationMessageDTO;

import java.util.HashMap;
import java.util.Map;

public class LocationCache implements Runnable {
    private static LocationCache instance;
    private Map<Integer, ProductWithLocation> cache;

    private LocationCache() {
        cache = new HashMap<>();
    }

    public synchronized static LocationCache getInstance() {
        if (instance == null) {
            return new LocationCache();
        }
        return instance;
    }

    public void run() {
        cache = new HashMap<>();
    }

    public void addLocation(ProductWithLocation location){
        cache.putIfAbsent(location.getProductId(), location);
    }

    public boolean isProductCached(int productId) {
        return cache.containsKey(productId);
    }

    public ProductWithLocation getProductLocation(int productId) {
        return cache.get(productId);
    }
}
