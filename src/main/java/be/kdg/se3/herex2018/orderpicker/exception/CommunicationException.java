package be.kdg.se3.herex2018.orderpicker.exception;

public class CommunicationException extends Exception {
    public CommunicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommunicationException(String message) {
        super(message);
    }
}
