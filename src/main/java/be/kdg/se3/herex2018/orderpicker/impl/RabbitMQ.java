package be.kdg.se3.herex2018.orderpicker.impl;

import be.kdg.se3.herex2018.gen.model.CancelMessageDTO;
import be.kdg.se3.herex2018.gen.model.OrderMessageDTO;
import be.kdg.se3.herex2018.orderpicker.api.Formatter;
import be.kdg.se3.herex2018.orderpicker.api.MessageService;
import be.kdg.se3.herex2018.orderpicker.exception.CommunicationException;
import be.kdg.se3.herex2018.orderpicker.listeners.CancelMessageListener;
import be.kdg.se3.herex2018.orderpicker.listeners.OrderMessageListener;
import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

// TODO: 8/2/18 als er nog tijd is, maak een generieke MessageListener
public class RabbitMQ implements MessageService {
    private final String QUEUENAME;
    private final String CONNECTION_STRING;
    private final Formatter formatter;

    private Connection connection;
    private Channel channel;

    private final Logger logger = LoggerFactory.getLogger(RabbitMQ.class);
    private final String TAG = "Debug";

    public RabbitMQ(String QUEUENAME, String CONNECTION_STRING, Formatter formatter) {
        this.QUEUENAME = QUEUENAME;
        this.CONNECTION_STRING = CONNECTION_STRING;
        this.formatter = formatter;
    }
    @Override
    public void init(OrderMessageListener listener) throws CommunicationException {
        try {
            init();

            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                        throws IOException {
                    logger.info("Received message from RabbitMQ queue " + QUEUENAME);
                    String content = new String(body, "UTF-8");
                    logger.debug("MessageService content: " + content);
                    OrderMessageDTO messageDTO = null;

                    if (listener != null) {
                        try {
                            messageDTO = (OrderMessageDTO) formatter.unwrap(content);

                            listener.onReceive(messageDTO);
                            logger.info("Delivered message to listener");
                        } catch (Exception e) {
                            logger.error("Exception during callback to listener", e);
                        }
                    }
                }
            };
            channel.basicConsume(QUEUENAME, true, consumer);
        } catch(Exception e) {
            throw new CommunicationException(String.format("Error while initializing the RabbitMQ channel: %s", QUEUENAME), e);
        }
    }

    @Override
    public void init(CancelMessageListener listener) throws CommunicationException {
        try {
            init();

            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                        throws IOException {
                    logger.info("Received message from RabbitMQ queue " + QUEUENAME);
                    String content = new String(body, "UTF-8");

                    logger.debug("MessageService content: " + content);
                    if (listener != null) {
                        try {
                            CancelMessageDTO messageDTO = (CancelMessageDTO) formatter.unwrap(content);

                            listener.onReceive(messageDTO);
                            logger.info("Delivered message to listener");
                        } catch (Exception e) {
                            logger.error("Exception during callback to listener", e);
                        }
                    }
                }
            };
            channel.basicConsume(QUEUENAME, true, consumer);
        } catch(Exception e) {
            throw new CommunicationException(String.format("Error while initializing the RabbitMQ channel: %s", QUEUENAME), e);
        }
    }

    @Override
    public void init() throws CommunicationException {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(CONNECTION_STRING);

            factory.setRequestedHeartbeat(30);
            factory.setConnectionTimeout(30000);

            connection = factory.newConnection();
            channel = connection.createChannel();

            channel.queueDeclare(QUEUENAME,
                    false,
                    false,
                    false,
                    null);

            logger.info("Using uri '" + CONNECTION_STRING + "'");
            logger.info("Using queue '" + QUEUENAME + "'");

        } catch (Exception e) {
            throw new CommunicationException("Error while initializing a RabbitMQ channel", e);
        }
    }

    @Override
    public void sendMesssage(Object o) throws CommunicationException {
        try {
            String message = formatter.wrap(o);

            channel.basicPublish("", QUEUENAME, null, message.getBytes());
            logger.info("message send: " + message);
        } catch(Exception e) {
            logger.error("Error while trying to send the message", e);
        }
    }

    @Override
    public void shutdown() throws CommunicationException {
        try {
            channel.close();
            connection.close();
        } catch (IOException | TimeoutException e) {
            throw new CommunicationException("Unable to close connection to RabbitMQ", e);
        }
    }
}
