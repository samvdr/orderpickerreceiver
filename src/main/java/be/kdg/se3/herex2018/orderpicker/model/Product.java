package be.kdg.se3.herex2018.orderpicker.model;

/**
 * An class representing a product which will be part of an order.
 */
public class Product {
    private int productId;
    private int productItemAmount;

    public Product(int productId, int productItemAmount) {
        this.productId = productId;
        this.productItemAmount = productItemAmount;
    }

    public int getProductId() {
        return productId;
    }

    public int getProductItemAmount() {
        return productItemAmount;
    }

    public void setProductItemAmount(int productItemAmount) {
        this.productItemAmount = productItemAmount;
    }
}
