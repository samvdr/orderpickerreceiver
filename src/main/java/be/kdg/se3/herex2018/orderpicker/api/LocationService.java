package be.kdg.se3.herex2018.orderpicker.api;

import be.kdg.se3.herex2018.orderpicker.exception.CommunicationException;

public interface LocationService {
    String getLocation(int productId) throws CommunicationException;
}
