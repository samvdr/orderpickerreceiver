package be.kdg.se3.herex2018.orderpicker.impl;

import be.kdg.se3.herex2018.orderpicker.api.Formatter;
import be.kdg.se3.herex2018.orderpicker.exception.FormatException;
import be.kdg.se3.herex2018.orderpicker.model.dto.ErrorMessageDTO;
import be.kdg.se3.herex2018.orderpicker.model.dto.LocationMessageDTO;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.*;
import java.io.StringReader;
import java.io.StringWriter;

public class JsonFormatter implements Formatter {
    private static final Logger logger = LoggerFactory.getLogger(JsonFormatter.class);
    private Gson gson;

    public JsonFormatter() {
        this.gson = new Gson();
    }

    @Override
    public String wrap(Object o) throws FormatException {
        StringWriter stringWriter = new StringWriter();
        logger.info("An object is being formatted into a json string");

        String message = gson.toJson(o);

        logger.debug(String.format("message content: %s", message));
        return message;
    }

    @Override
    public Object unwrap(String message) throws FormatException {
        try {
            StringReader stringReader = new StringReader(message);
            JsonReader reader = Json.createReader(stringReader);

            JsonObject object = reader.readObject();
            if (object.getString("error").equals("") || object.getString("error").isEmpty()) {
                LocationMessageDTO messageDTO = (LocationMessageDTO) object;
                return messageDTO;
            } else {
                ErrorMessageDTO messageDTO = (ErrorMessageDTO) object;
                return messageDTO;
            }
        } catch(Exception e) {
            throw new FormatException(String.format("Couldn't format a message: %s", message), e);
        }
    }
}
