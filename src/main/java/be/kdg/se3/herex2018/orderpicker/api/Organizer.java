package be.kdg.se3.herex2018.orderpicker.api;

import be.kdg.se3.herex2018.orderpicker.model.OrderMessageWithLocations;
import be.kdg.se3.herex2018.orderpicker.model.dto.OrderMessagesWithLocationsDTO;

public interface Organizer {
    OrderMessagesWithLocationsDTO organize(OrderMessageWithLocations orderMessageWithLocations);

    boolean isOrderPresent(int orderId);

    void removeOrder(int orderId);
}
