package be.kdg.se3.herex2018.orderpicker.exception;

public class FormatException extends Exception {
    public FormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
