package be.kdg.se3.herex2018.orderpicker.model.dto;

public class ErrorMessageDTO {
    private final String error;
    private final String description;

    public ErrorMessageDTO(String error, String description) {
        this.error = error;
        this.description = description;
    }

    public String getError() {
        return error;
    }

    public String getDescription() {
        return description;
    }
}
