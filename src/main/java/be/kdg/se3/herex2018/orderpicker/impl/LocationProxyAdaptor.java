package be.kdg.se3.herex2018.orderpicker.impl;

import be.kdg.se3.herex2018.orderpicker.api.LocationService;
import be.kdg.se3.herex2018.orderpicker.exception.CommunicationException;
import be.kdg.se3.services.orderpicking.LocationServiceProxy;

import java.io.IOException;

public class LocationProxyAdaptor implements LocationService {
    private LocationServiceProxy service;

    public LocationProxyAdaptor(LocationServiceProxy service) {
        this.service = service;
    }

    @Override
    public String getLocation(int productId) throws CommunicationException {
        try {
            return service.get(String.format("www.services4se3.com/locationservice/%d", productId));
        } catch (IOException e) {
            throw new CommunicationException(String.format("The location service is temporarily unreachable for the roduct with id: %d.", productId), e);
        }
    }
}
