package be.kdg.se3.herex2018.orderpicker.listeners;


import be.kdg.se3.herex2018.gen.model.OrderMessageDTO;

public interface OrderMessageListener {
    void onReceive(OrderMessageDTO messageDTO);
}
