package be.kdg.se3.herex2018.orderpicker.model;

public class ProductWithLocation {
    private final int productId;
    private final int productItemAmount;
    private final String storageRoom;
    private final int hallway;
    private final int rack;

    public ProductWithLocation(int productId, int productItemAmount, String storageRoom, int hallway, int rack) {
        this.productId = productId;
        this.productItemAmount = productItemAmount;
        this.storageRoom = storageRoom;
        this.hallway = hallway;
        this.rack = rack;
    }

    public int getProductId() {
        return productId;
    }

    public int getProductItemAmount() {
        return productItemAmount;
    }

    public String getStorageRoom() {
        return storageRoom;
    }

    public int getHallway() {
        return hallway;
    }

    public int getRack() {
        return rack;
    }
}
