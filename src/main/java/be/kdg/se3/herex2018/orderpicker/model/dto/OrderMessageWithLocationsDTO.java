package be.kdg.se3.herex2018.orderpicker.model.dto;

import be.kdg.se3.herex2018.orderpicker.model.ProductWithLocation;

import java.util.List;

public class OrderMessageWithLocationsDTO {
    private final int orderId;
    private final List<ProductWithLocation> items;

    public OrderMessageWithLocationsDTO(int orderId, List<ProductWithLocation> items) {
        this.orderId = orderId;
        this.items = items;
    }

    public int getOrderId() {
        return orderId;
    }

    public List<ProductWithLocation> getItems() {
        return items;
    }
}
