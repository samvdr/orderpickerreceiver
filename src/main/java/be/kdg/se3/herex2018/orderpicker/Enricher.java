package be.kdg.se3.herex2018.orderpicker;

import be.kdg.se3.herex2018.gen.model.OrderMessageDTO;
import be.kdg.se3.herex2018.gen.model.Product;
import be.kdg.se3.herex2018.orderpicker.api.LocationService;
import be.kdg.se3.herex2018.orderpicker.exception.CommunicationException;
import be.kdg.se3.herex2018.orderpicker.exception.ErrorMessageException;
import be.kdg.se3.herex2018.orderpicker.model.ProductWithLocation;
import be.kdg.se3.herex2018.orderpicker.model.dto.ErrorMessageDTO;
import be.kdg.se3.herex2018.orderpicker.model.dto.LocationMessageDTO;
import be.kdg.se3.herex2018.orderpicker.model.OrderMessageWithLocations;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class Enricher {
    private final LocationService service;
    private Gson gson;
    private LocationCache cache;

    public Enricher(LocationService service) {
        this.service = service;
        this.gson = new Gson();
        this.cache = LocationCache.getInstance();
    }

    public OrderMessageWithLocations addLocations(OrderMessageDTO orderMessage) throws CommunicationException, ErrorMessageException {
        String locationJson;
        List<ProductWithLocation> products = new ArrayList<>(orderMessage.getItems().size());
        LocationMessageDTO locationMessage = null;

        for (Product product : orderMessage.getItems()) {
            ProductWithLocation productWithLocation = null;
            if (cache.isProductCached(product.getProductId())) {
                productWithLocation = cache.getProductLocation(product.getProductId());
            } else {
                locationJson = service.getLocation(product.getProductId());
                try {
                    locationMessage = gson.fromJson(locationJson, LocationMessageDTO.class);

                    if (locationMessage != null) {
                        productWithLocation = new ProductWithLocation(locationMessage.getProductID(),
                                product.getProductItemAmount(),
                                locationMessage.getStorageRoom(),
                                locationMessage.getHallway(),
                                locationMessage.getRack());
                    }
                } catch (Exception e) {
                    ErrorMessageDTO errorMessageDTO = gson.fromJson(locationJson, ErrorMessageDTO.class);
                    throw new ErrorMessageException(errorMessageDTO.getDescription(), e);
                }
            }
            products.add(productWithLocation);
        }

        return new OrderMessageWithLocations(orderMessage.getOrderId(), products);
    }
}
