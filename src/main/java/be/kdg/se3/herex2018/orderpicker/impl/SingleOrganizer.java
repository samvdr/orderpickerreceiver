package be.kdg.se3.herex2018.orderpicker.impl;

import be.kdg.se3.herex2018.orderpicker.api.Organizer;
import be.kdg.se3.herex2018.orderpicker.model.OrderMessageWithLocations;
import be.kdg.se3.herex2018.orderpicker.model.ProductWithLocation;
import be.kdg.se3.herex2018.orderpicker.model.dto.OrderMessageWithLocationsDTO;
import be.kdg.se3.herex2018.orderpicker.model.dto.OrderMessagesWithLocationsDTO;

import java.util.ArrayList;
import java.util.List;

public class SingleOrganizer implements Organizer {
    @Override
    public OrderMessagesWithLocationsDTO organize(OrderMessageWithLocations orderMessageWithLocations) {
        List<ProductWithLocation> products = getSortedProducts(orderMessageWithLocations);
        List<OrderMessageWithLocationsDTO> messages = new ArrayList<>();
        messages.add(new OrderMessageWithLocationsDTO(orderMessageWithLocations.getOrderId(), products));

        return new OrderMessagesWithLocationsDTO(messages);
    }

    @Override
    public boolean isOrderPresent(int orderId) {
        return false;
    }

    @Override
    public void removeOrder(int orderId) {
        // TODO: 10/08/2018 log error
    }

    private List<ProductWithLocation> getSortedProducts(OrderMessageWithLocations orderMessageWithLocations) {
        List<ProductWithLocation> products = orderMessageWithLocations.getItems();

        products.sort(null);
        return products;
    }
}
