package be.kdg.se3.herex2018.orderpicker.model.dto;

public class LocationMessageDTO {
    private final int productID;
    private final String storageRoom;
    private final int hallway;
    private final int rack;

    public LocationMessageDTO(int productID, String storageRoom, int hallway, int rack) {
        this.productID = productID;
        this.storageRoom = storageRoom;
        this.hallway = hallway;
        this.rack = rack;
    }

    public int getProductID() {
        return productID;
    }

    public String getStorageRoom() {
        return storageRoom;
    }

    public int getHallway() {
        return hallway;
    }

    public int getRack() {
        return rack;
    }
}
