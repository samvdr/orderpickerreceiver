package be.kdg.se3.herex2018.orderpicker.impl;

import be.kdg.se3.herex2018.orderpicker.api.Organizer;
import be.kdg.se3.herex2018.orderpicker.model.OrderMessageWithLocations;
import be.kdg.se3.herex2018.orderpicker.model.dto.OrderMessageWithLocationsDTO;
import be.kdg.se3.herex2018.orderpicker.model.dto.OrderMessagesWithLocationsDTO;

import java.util.*;

public class GroupOrganizer implements Organizer {
    private Organizer organizer;
    private Map<Integer, OrderMessageWithLocations> buffer;
    private int bufferSize;

    public GroupOrganizer(Organizer organizer) {
        this.organizer = organizer;
        this.bufferSize = 10;
        this.buffer = new HashMap<>(bufferSize);
    }

    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
        buffer = new HashMap<>(bufferSize);
    }

    @Override
    public OrderMessagesWithLocationsDTO organize(OrderMessageWithLocations orderMessageWithLocations) {
        if (buffer.size() < bufferSize) {
            buffer.put(orderMessageWithLocations.getOrderId(), orderMessageWithLocations);
            if (buffer.size() == bufferSize) {
                OrderMessagesWithLocationsDTO orderMessagesWithLocationsDTO = HandleBuffer();
                buffer = new HashMap<>();

                return orderMessagesWithLocationsDTO;
            }
        }

        return null;
    }

    private OrderMessagesWithLocationsDTO HandleBuffer() {
        Map<Integer, OrderMessageWithLocations> orderMap = buffer;
        buffer = new HashMap<>(bufferSize);
        Set<Integer> keys = orderMap.keySet();
        List<OrderMessageWithLocationsDTO> orders = new ArrayList<>(bufferSize);

        for (Integer key : keys) {
            orders.add(
                    organizer.organize(orderMap.get(key)).getOrders().get(0)
            );
        }

        return new OrderMessagesWithLocationsDTO(orders);
    }

    @Override
    public boolean isOrderPresent(int orderId) {
        return buffer.containsKey(orderId);
    }

    @Override
    public void removeOrder(int orderId) {
        buffer.remove(orderId);
    }
}
