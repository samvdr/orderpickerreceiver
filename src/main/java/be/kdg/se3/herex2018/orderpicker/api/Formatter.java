package be.kdg.se3.herex2018.orderpicker.api;

import be.kdg.se3.herex2018.orderpicker.exception.FormatException;

public interface Formatter {
    String wrap(Object o) throws FormatException;

    Object unwrap(String message) throws FormatException;
}
