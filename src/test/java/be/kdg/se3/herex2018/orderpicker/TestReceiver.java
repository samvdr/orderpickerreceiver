package be.kdg.se3.herex2018.orderpicker;

import be.kdg.se3.herex2018.orderpicker.api.Formatter;
import be.kdg.se3.herex2018.orderpicker.api.MessageService;
import be.kdg.se3.herex2018.orderpicker.exception.CommunicationException;
import be.kdg.se3.herex2018.orderpicker.impl.RabbitMQ;
import be.kdg.se3.herex2018.orderpicker.impl.XMLFormatter;
import be.kdg.se3.herex2018.orderpicker.listeners.CancelMessageListener;
import be.kdg.se3.herex2018.orderpicker.listeners.OrderMessageListener;

import java.util.Scanner;

public class TestReceiver {
    public static void main(String[] args) {
        Formatter xmlFormatter = new XMLFormatter();
        MessageService cancelChanel = new RabbitMQ("CancelMessages", "localhost", xmlFormatter);
        MessageService orderChanel = new RabbitMQ("OrderMessages", "localhost", xmlFormatter);
        try {
            cancelChanel.init((CancelMessageListener) (dto) -> {
                System.out.println(dto.getOrderId());
            });
        } catch (CommunicationException e) {
            e.printStackTrace();
        }
        try {
            orderChanel.init((OrderMessageListener) (dto) -> {
                System.out.println(dto.getOrderId());
            });
        } catch (CommunicationException e) {
            e.printStackTrace();
        }
        new Scanner(System.in).nextLine();
    }
}
